# Proyecto AEM

Esta es una plantilla de proyecto para aplicaciones basadas en AEM. Está pensado como un conjunto de ejemplos de mejores prácticas, así como un punto de partida potencial para desarrollar su propia funcionalidad.

### Módulos

Las partes principales de AEM archetype  son:

* core: paquete de Java que contiene todas las funciones básicas como los servicios OSGi, listeners o schedulers, así como código Java relacionado con componentes, como  servlets or request filters.
* ui.apps: contiene las partes / apps (y / etc) del proyecto, es decir, las bibliotecas de cliente JS y CSS, los componentes, las plantillas, las configuraciones específicas del modo de ejecución y las pruebas de Hobbes
* ui.content: contiene contenido de muestra utilizando los componentes de ui.apps
* ui.tests: paquete de Java que contiene pruebas JUnit que se ejecutan en el lado del servidor. Este paquete no se debe implementar en producción.
* ui.launcher: contiene código adhesivo que implementa el paquete ui.tests (y los paquetes dependientes) en el servidor y activa la ejecución remota de JUnit
* ui.frontend: un mecanismo de compilación de front-end dedicado opcional basado en Webpack
* all: este es un módulo Maven vacío que combina los módulos anteriores en un solo paquete que se puede implementar en un entorno AEM.

![](https://docs.adobe.com/content/dam/help/experience-manager-learn.en/help/getting-started-wknd-tutorial-develop/assets/project-setup/project-pom-structure.png)


###Construye el proyecto

Ahora que hemos generado un nuevo proyecto, podemos implementar el código del proyecto en una instancia local de AEM.

Asegúrese de tener una instancia de AEM ejecutándose localmente en el puerto 4502.
Desde la línea de comandos, navegue hasta el directorio del proyecto aem-guides-wknd.

>cd aem-guides-wknd

Para construir todos los módulos, ejecute en el directorio raíz del proyecto el siguiente comando con Maven 3:

>mvn clean install

Si tiene una instancia de AEM en ejecución, puede compilar y empaquetar todo el proyecto e implementarlo en AEM con

>mvn clean install -PautoInstallPackage

El perfil de Maven ***autoInstallSinglePackage*** compila los módulos individuales del proyecto e implementa un solo paquete en la instancia de AEM. De forma predeterminada, este paquete se implementará en una instancia de AEM que se ejecuta localmente en el puerto 4502 y con las credenciales de **admin: admin**

Para implementarlo en una instancia de publicación, ejecute

>mvn clean install -PautoInstallPackagePublish

O alternativamente si queremos explicitar el puerto

>mvn clean install -PautoInstallPackage -Daem.port = 4503

O para implementar solo el paquete al autor, ejecute

>mvn clean install -PautoInstallBundle

Vamos al Administrador de paquetes en su instancia de AEM local: 
[http://localhost:4502/crx/packmgr/index.jsp](http://localhost:4502/crx/packmgr/index.jsp) 

![](https://i.imgur.com/8lykLNA.png)

Debería ver tres paquetes para aem-guides-wknd.ui.apps, aem-guides-wknd.ui.content y aem-guides-wknd.all.

También debería ver varios paquetes para AEM Core Components que están incluidos en el proyecto por el arquetipo.

Navegue a la consola de Sitios:[http://localhost:4502/sites.html/content](http://localhost:4502/sites.html/content). El sitio WKND será uno de los sitios. Incluirá una estructura de sitio con una jerarquía de Maestros de idiomas y de EE. UU. Esta jerarquía de sitios se basa en los valores de *language_country* e *isSingleCountryWebsite* al generar el proyecto utilizando el arquetipo.

Abra la página en inglés de US. Seleccionando la página y haciendo clic en el botón Editar en la barra de menú:

![](https://docs.adobe.com/content/dam/help/experience-manager-learn.en/help/getting-started-wknd-tutorial-develop/assets/project-setup/aem-sites-console.png)


### Pruebas

Hay tres niveles de prueba contenidos en el proyecto:

* prueba unitaria en el núcleo: este espectáculo-
casos de pruebas unitarias clásicas del código contenido en el paquete. Para probar, ejecute:

>mvn clean test

* Pruebas de integración del lado del servidor: esto permite ejecutar pruebas tipo unidad en el entorno AEM, es decir, en el servidor AEM. Para probar, ejecute:

>mvn clean verify -PintegrationTests

* Pruebas Hobbes.js del lado del cliente: navegador basado en JavaScript-
pruebas complementarias que verifican el comportamiento del navegador. Probar:

    en el navegador, abra la página en 'Modo de desarrollador', abra el panel izquierdo y cambie a la pestaña 'Pruebas' y busque las 'Pruebas de MyName' generadas y ejecútelas.

### Configuración de Maven

El proyecto viene con el repositorio público automático configurado. Para configurar el repositorio en la configuración de Maven,
Referirse a:

    http://helpx.adobe.com/experience-manager/kb/SetUpTheAdobeMavenRepository.html
    
### Compilando por modulos

Durante el desarrollo, es posible que esté trabajando con solo uno de los módulos y desee evitar la construcción del proyecto completo para ahorrar tiempo. Es posible que también desee implementar directamente en una instancia de AEM Publish o quizás en una instancia de AEM que no se ejecuta en el puerto 4502.

A continuación, veremos algunos perfiles y comandos de Maven adicionales que puede usar para una mayor flexibilidad durante el desarrollo.

#### Core module

El módulo principal contiene todo el código Java asociado con el proyecto. Cuando se construye, implementa un paquete OSGi en AEM. Para construir solo este módulo:

>$ cd core/


>$ mvn -PautoInstallBundle clean install

Vaya a [http://localhost:4502/system/console/bundles](http://localhost:4502/system/console/bundles) Esta es la consola web de OSGi y contiene información sobre todos los paquetes instalados en la instancia de AEM.

Cambie la columna de clasificación de Id y debería ver el paquete WKND instalado y activo.

![](https://docs.adobe.com/content/dam/help/experience-manager-learn.en/help/getting-started-wknd-tutorial-develop/assets/project-setup/wknd-osgi-console.png)

puedemos ver la locacion fisica del jar [aqui](http://localhost:4502/crx/de/index.jsp#/apps/wknd/install/wknd-sites-guide.core-0.0.1-SNAPSHOT.jar) 

![](https://i.imgur.com/Gvx74dh.png)

#### Módulos Ui.apps y Ui.content

El módulo ui.apps maven contiene todo el código de renderizado necesario para el sitio debajo de /apps. Esto incluye CSS/JS que se almacenará en un formato AEM llamado clientlibs.

Esto también incluye scripts HTL para renderizar HTML dinámico. Puede pensar en el módulo ui.apps como un mapa de la estructura en el JCR pero en un formato que puede almacenarse en un sistema de archivos y comprometerse con el control de fuente. El módulo ui.apps solo contiene código.

Para construir solo este módulo:

Desde la línea de comando. Navegue a la carpeta ui.apps (debajo de aem-guides-wknd):

>cd ../ui.apps


>mvn -PautoInstallPackage clean install

Vamos a (http://localhost:4502/crx/packmgr/index.jsp)[http://localhost:4502/crx/packmgr/index.jsp] Debería ver el paquete ui.apps como el primer paquete instalado y debería tener una marca de tiempo más reciente que cualquiera de los otros paquetes.

![](https://docs.adobe.com/content/dam/help/experience-manager-learn.en/help/getting-started-wknd-tutorial-develop/assets/project-setup/ui-apps-package.png)

Regresamos a la línea de comando y ejecutamos el siguiente comando (dentro de la carpeta ui.apps):

>mvn -PautoInstallPackagePublish clean install

El perfil autoInstallPackagePublish está diseñado para implementar el paquete en un entorno de publicación que se ejecuta en el puerto 4503. Se espera el error anterior si no se puede encontrar una instancia de AEM que se ejecuta en http://localhost:4503.

Finalmente, ejecute el siguiente comando para implementar el paquete ui.apps en el puerto 4504:

>mvn -PautoInstallPackage clean install -Daem.port=4504

Nuevamente, se espera que ocurra un error de compilación si no hay disponible una instancia de AEM que se ejecute en el puerto 4504. El parámetro aem.port se define en el archivo POM en aem-guides-wknd/pom.xml

El módulo ui.content está estructurado de la misma manera que el módulo ui.apps. La única diferencia es que el módulo ui.content contiene lo que se conoce como contenido mutable. El contenido mutable se refiere esencialmente a configuraciones no-código como templates, Políticas o estructuras de carpetas que se almacenan en código fuente-control, pero podría modificarse directamente en una instancia de AEM.

Esto se explorará con mucho más detalle en el capítulo sobre páginas y plantillas. Por ahora, la conclusión importante es que los mismos comandos de Maven que se usaron para construir el módulo ui.apps se pueden usar para construir el módulo ui.content. No dude en repetir los pasos anteriores desde la carpeta ui.content.

#### Modulo Ui.frontend

El módulo ui.frontend es un módulo de Maven que en realidad es un proyecto  webpack project. Este módulo está configurado para ser un sistema de compilación de front-end dedicado que genera archivos JavaScript y CSS, que a su vez se implementan en AEM. 

El módulo ui.frontend permite a los desarrolladores codificar con lenguajes como Sass, TypeScript, usar módulos npm e integrar la salida directamente en AEM.

El módulo ui.frontend se cubrirá con mucho más detalle en el capítulo sobre bibliotecas del lado del cliente y desarrollo de front-end. Por ahora veamos cómo se integra en el proyecto.

Desde la línea de comando. Navegue a la carpeta ui.frontend (debajo de aem-guides-wknd):

>$ cd ../ui.frontend

>mvn clean install

Observe las líneas como copy: dist/clientlib-site/site.js ../ui.apps/src/main/content/jcr_root/apps/wknd/clientlibs/clientlib-site/js/site.js. Esto indica que el CSS y JS compilados se están copiando en la carpeta ui.apps.


Vea la marca de tiempo modificada para el archivo *aem-guides-wknd/ui.apps/src/main/content/jcr_root/apps/wknd/clientlibs/clientlib-site/css.txt* Debería actualizarse más recientemente que los demás archivos del módulo ui.apps.

A diferencia de los otros módulos que analizamos, el módulo ui.frontend no se implementa directamente en AEM. En su lugar, CSS y JS se copian en el módulo ui.apps y luego el módulo ui.apps se implementa en AEM. Si observa el orden de compilación desde el primer comando de Maven, verá que ui.frontend siempre se compila antes que ui.apps.

### Inclusión de componentes básicos


El arquetipo integra automáticamente los componentes principales de AEM en el proyecto. Anteriormente, al inspeccionar los paquetes implementados en AEM, se incluyeron varios paquetes relacionados con los componentes principales. Los componentes principales son un conjunto de componentes básicos diseñados para acelerar el desarrollo de un proyecto de AEM Sites.
Los componentes principales son de código abierto y están disponibles en GitHub. Puede encontrar más información sobre cómo se incluyen los componentes principales en el proyecto aquí.
Con su editor de texto favorito, abra aem-guides-wknd / pom.xml.
Busque core.wcm.components.version. Esto le mostrará qué versión de los componentes principales está incluida:

```xml
<core.wcm.components.versión>2.x.x</core.wcm.components.version>
```
El arquetipo del proyecto AEM incluirá una versión de los componentes principales de AEM; sin embargo, estos proyectos tienen ciclos de lanzamiento diferentes y, por lo tanto, la versión incluida de los componentes principales puede no ser la última. Como práctica recomendada, siempre debe buscar aprovechar la última versión de los componentes principales.

Las nuevas funciones y las correcciones de errores se actualizan con frecuencia. La información de la última versión se puede encontrar en GitHub.
Si se desplaza hacia abajo hasta la sección de dependencias, debería ver las dependencias individuales del componente principal:


[VerVideo](https://images-tv.adobe.com/mpcv3/9e83e988-4abe-4905-97bd-32ae635bad66_1575937759.1920x1080at3000_h264.mp4)

<video width="320" height="240" controls>
  <source src="https://images-tv.adobe.com/mpcv3/9e83e988-4abe-4905-97bd-32ae635bad66_1575937759.1920x1080at3000_h264.mp4" type="video/mp4">
</video>


Para el resto de capitulos cambia el branch y continua con la lectura

